﻿using System;

namespace Interview.Entities
{
    public class ProductId:IComparable
    {
        public string Barcode { get; set; }
        public string BatchNumber { get; set; }

        public int CompareTo(object obj)
        {
            var prodId=(ProductId)obj;
            if (Barcode == prodId.Barcode && BatchNumber == prodId.BatchNumber)
            {
                return 0;
            }

            return 1;
        }
    }
}
