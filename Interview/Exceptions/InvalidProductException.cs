﻿using System;

namespace Interview.Exceptions
{
    public class InvalidProductException : Exception
    {
        public InvalidProductException(string message) : base(message)
        {
        }
    }
}
