﻿using System.Collections.Generic;

namespace Interview.DbInteractions
{
    public interface IDbInteraction<TEntity>
    {
        IList<TEntity> Entities { get; }
    }
}
