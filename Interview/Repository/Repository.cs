﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interview.DbInteractions;
using Interview.Exceptions;

namespace Interview.Repository
{
    public class Repository<T, I> : IRepository<T, I> where T:IStoreable<I> where I:IComparable
    {
        private readonly IDbInteraction<T> _dbInteraction;

        public Repository(IDbInteraction<T> dbInteraction)
        {
            _dbInteraction = dbInteraction;
        }

        public void Delete(I id)
        {
            var item = Get(id);
            if (item == null)
            {
                throw new InvalidProductException("You Tried to Delete Invalid Product!");
            }
            _dbInteraction.Entities.Remove(item);
        }

        public T Get(I id)
        {
            return _dbInteraction.Entities.FirstOrDefault(x=>x.Id.CompareTo(id)==0);
        }

        public IEnumerable<T> GetAll()
        {
            return _dbInteraction.Entities;
        }

        public void Save(T item)
        {
            if (item==null || item.Id==null)
            {
                throw new InvalidProductException("You Tried to save Invalid Product Information!");
            }
            if(Get(item.Id)==null)
             _dbInteraction.Entities.Add(item);
        }
    }
}
