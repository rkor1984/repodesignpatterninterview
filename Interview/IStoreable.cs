﻿using System;

namespace Interview
{
    public interface IStoreable<T> where T : IComparable
    {
        T Id { get; set; }
    }
    
}