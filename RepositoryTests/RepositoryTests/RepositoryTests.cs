﻿using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using Interview.DbInteractions;
using Interview.Entities;
using Interview.Exceptions;
using Interview.Repository;
using Shouldly;
using Xunit;

namespace RepositoryTests.RepositoryTests
{
    public class RepositoryTests:IDbInteraction<Product>
    {
        private readonly Repository<Product,ProductId> _repo;
        private readonly Fixture _fixture;

        public RepositoryTests()
        {
            _fixture = new Fixture();

            InitializeData();
            _repo = new Repository<Product, ProductId>(this);
        }

        [Theory, InlineData("Barcode11", "BatchNumber11")]
        public void GetByIdBringsNullForNonExistingData(string barcode, string batchNumber)
        {
            var productId = new ProductId
            {
                Barcode = barcode,
                BatchNumber = batchNumber
            };
            var product = _repo.Get(productId);
            product.ShouldBeNull();
        }

        [Theory, InlineData("barcode2", "batchNumber1")]
        public void GetByIdBringsBackRecordForExistingData(string barcode, string batchNumber)
        {
            var productId = new ProductId
            {
                Barcode = barcode,
                BatchNumber = batchNumber
            };
            var product = _repo.Get(productId);
            product.ShouldNotBeNull();
        }

        [Fact]
        public void GetAllBringsBackAllRecords()
        {
            var allProd = _repo.GetAll();
            allProd.Count().ShouldBe(Entities.Count);
        }

        [Fact]
        public void SaveIncrementsNumberByOneForValidData()
        {
            var newProd = _fixture.Build<Product>().Create();
            var countBeforeAdding = Entities.Count();

            _repo.Save(newProd);

            var countAfterAdding = Entities.Count();

            countAfterAdding.ShouldBe(countBeforeAdding+1);
        }

        [Theory, InlineData("barcode2", "batchNumber1")]
        public void DoNotSaveForDuplicateData(string barcode, string batchNumber)
        {
            var newProd = new Product
            {
                Id = new ProductId
                {
                    Barcode = barcode,
                    BatchNumber = batchNumber
                }
            };
            var countBeforeAdding = Entities.Count();

            _repo.Save(newProd);

            var countAfterAdding = Entities.Count();

            countAfterAdding.ShouldBe(countBeforeAdding);
        }

        [Fact]
        public void SaveThrowsExceptionForInvalidData()
        {
            Assert.Throws<InvalidProductException>(()=>_repo.Save(null));
            Assert.Throws<InvalidProductException>(()=>_repo.Save(new Product
            {
                Id = null
            }));
        }
      
        [Theory,InlineData("barcode4", "batchNumber3")]
        public void DeleteWorksForValidExistingData(string barcode, string batch)
        {
            var id = new ProductId
            {
                Barcode = barcode,
                BatchNumber = batch
            };
            var beforeDeleteCount = Entities.Count();

            _repo.Delete(id);

            var afterDeleteCount = Entities.Count();

            afterDeleteCount.ShouldBe(beforeDeleteCount-1);
        }

        [Theory,InlineData("barcode","batch")]
        public void DeleteThrowsExceptionForInvalidData(string barcode,string batch)
        {
            var id = new ProductId
            {
                Barcode = barcode,
                BatchNumber = batch
            };

            Assert.Throws<InvalidProductException>(() => _repo.Delete(id));
        }

        public IList<Product> Entities { get; set; }
        private void InitializeData()
        {
            Entities = new List<Product>
            {
                new Product
                {
                    Id = new ProductId
                    {
                        Barcode = "barcode1",
                        BatchNumber = "batchNumber1"
                    }
                },
                new Product
                {
                    Id = new ProductId
                    {
                        Barcode = "barcode2",
                        BatchNumber = "batchNumber1"
                    }
                },
                new Product
                {
                    Id = new ProductId
                    {
                        Barcode = "barcode3",
                        BatchNumber = "batchNumber2"
                    }
                },
                new Product
                {
                    Id = new ProductId
                    {
                        Barcode = "barcode4",
                        BatchNumber = "batchNumber3"
                    }
                },
            };
        }
    }
}
